# FilmUniverseDB ASP.NET API

## Description
This is a simple example ASP.NET Core Web API to display and modify different characters, movies and franchises in a film universe database. The project is the third assignment for the Noroff fullstack .NET-course.
The assignment is split into two parts, with part A consisting of a code first approach for constructing the application in ASP.NET Core with a database made in SQL Server through EF Core with a RESTful API to allow users to manipulate the data.

Part B consists of writing API endpoints with full CRUD for the tables Movies, Characters and Franchises, as well as making data transfer objects (DTOs) for all the model classes clients interact with. The endpoints are built, designed and documented with the help of Swagger.

## Installation
Clone the project and simply run the program in Visual Studio. No database is provided as it will be built by executing the code. In order to make a connection to the database, you will need to use Microsoft SQL Server Management Studio, and copy the connection string from there. The connection string needs to be placed in a file called `appsettings.json` and be placed in the root folder of the project. The `appsettings.json` file should look like this:

```cs
{
  "Logging": {
    "LogLevel": {
      "Default": "Information",
      "Microsoft": "Warning",
      "Microsoft.Hosting.Lifetime": "Information"
    }
  },
    "AllowedHosts": "*",
    // Setting the connection string
  "ConnectionStrings": {
    "DefaultConnection": "Data Source=YOUR_CONNECTION_STRING_HERE;Initial Catalog=FilmUniverse; Integrated Security=true;"
  }
}
```

Replace `YOUR_CONNECTION_STRING_HERE` in line 12 with the connection string provided by SMSS.

## Part A
For part A, we created models and controllers for each of the tables in the database, found in `Model\` and `Controllers\` respectively. This part consisted mainly of setting up all the data and relations between the data as described in appendix A in the assignment.

The requirements for the data were as following:

* Character
    * Autoincremented Id
    * Full name
    * Alias (if applicable)
    * Gender
    * Picture (URL to photo – do not store an image)
* Movie
    * Autoincremented Id
    * Movie title
    * Genre (just a simple string of comma separated genres, there is no genre modelling required as a base)
    * Release year
    * Director (just a string name, no director modelling required as a base)
    * Picture (URL to a movie poster)
    * Trailer (YouTube link most likely)
* Franchise
    * Autoincremented Id
    * Name
    * Description

Which is reflected in `Model\` and `Profiles\`

The database is seeded with some sample data found in `Data\SeedingData.cs`

## Part B
For part B we implemented DTOs and services, as well as interfaces which describe which methods each service should implement. This meant refactoring the code found in the controllers in order to move anything that has to do with the dbcontext over to the services, and letting the controllers map the data gathered from the services. The profiles describe how the relation between each table and foreign keys are set up.

The API endpoints created include full CRUD for Movies, Characters and Franchises, as well as five dedicated endpoints as described in the assignment to:

* Update characters in a movie
* Update movies in a franchise
* Get all movies in a franchise
* Get all characters in a movie
* Get all characters in a franchise

We also created documentation for each of the endpoints of the API in swagger found in `Controllers\`

For this assignment we also had to create a CI pipeline to build our application as a Docker artifact. To do this we had to create a Dockerfile which contains all the instructions for building our application's Docker image. When we tried to build the image in Windows PowerShell we encountered a secure socket layer (SSL) connection. To deal with this issue we needed to export and install some certificates that we accessed by typing 'certmgr.msc' in the windows search bar.
##### Adding a custom certificate

The following steps are necessary to add a custom certificate (cert1.cer) to the list of trusted certificates:
1. Copy the certificate to the `/usr/share/ca-certificates/` directory:
COPY cert1.cer /usr/share/ca-certificates/

2. Add the certificate to the list of trusted certificates in `/etc/ca-certificates.conf`:
RUN echo cert1.cer >> /etc/ca-certificates.conf

3. Update the trusted certificate list:
RUN update-ca-certificates

With these instructions, the certificates (cert1.cer in our case) will be added to the list of trusted certificates in the Docker image.


## Contributors
This assignment as a whole was developed by Petter Dybdal ([petterdybdal](https://gitlab.com/petterdybdal)) and Tore Kamsvåg ([Torekamsvag](https://gitlab.com/Torekamsvag)).