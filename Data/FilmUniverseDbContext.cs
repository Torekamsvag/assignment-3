﻿using Assignment3.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;

namespace Assignment3.Data
{
    public class FilmUniverseDbContext : DbContext     // Inheriting from the DbContext class
    {
        public FilmUniverseDbContext(DbContextOptions options) : base(options)
        {
        }
        // DbSets for the tables in the database
        public DbSet<Character> Characters { get; set; }
        public DbSet<Franchise> Franchises { get; set; }
        public DbSet<Movie> Movies { get; set; }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            // Invoking the method to seed the data
            modelBuilder.Entity<Franchise>().HasData(SeedingData.GetFranchises());
            modelBuilder.Entity<Movie>().HasData(SeedingData.GetMovies());
            modelBuilder.Entity<Character>().HasData(SeedingData.GetCharacters());

            // Setting up the many-to-many relationship between movies and characters
            // Defining the relationship and accessing the linking table
            modelBuilder.Entity<Movie>()
                .HasMany(p => p.Characters)
                .WithMany(m => m.Movies)
                .UsingEntity<Dictionary<string, object>>(
                    "MovieCharacter",
                    r => r.HasOne<Character>().WithMany().HasForeignKey("CharacterId"),
                    l => l.HasOne<Movie>().WithMany().HasForeignKey("MovieId"),
                    joinEntity =>
                    {
                        // Defining the primary key for the linking table
                        joinEntity.HasKey("MovieId", "CharacterId");
                        // Seeding the data for the linking table
                        joinEntity.HasData(
                            new { MovieId = 1, CharacterId = 1 },
                            new { MovieId = 2, CharacterId = 1 },
                            new { MovieId = 3, CharacterId = 2 },
                            new { MovieId = 4, CharacterId = 3 }
                        );
                    });
        }


    }

}

