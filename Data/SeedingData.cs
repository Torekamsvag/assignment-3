﻿using Assignment3.Model;
using Microsoft.AspNetCore.DataProtection.AuthenticatedEncryption.ConfigurationModel;
using System.Collections.Generic;
using System.Reflection.Metadata.Ecma335;
using static System.Net.WebRequestMethods;

namespace Assignment3.Data
{
    public class SeedingData
    {   
        /// <summary>
        /// Method to get all franchises
        /// </summary>
        /// <returns>Returns a list of franchise objects</returns>
        public static List<Franchise> GetFranchises()
        {   
            
            List<Franchise> franchises = new List<Franchise>()
            {
                new Franchise() {
                    Id = 1, 
                    Name = "Marvel Cinematic Universe", 
                    Description = "Series of American Superhero films produced by Marvel Studios"
                },
                new Franchise() {
                    Id = 2, 
                    Name = "Middle-earth", 
                    Description = "Middle-earth is the fictional setting of much of the English writer J. R. R. Tolkien's fantasy. The Hobbit and The Lord of the Rings, are set entirely in Middle-earth."
                },
                new Franchise() {
                    Id = 3, 
                    Name = "Harry Potter", 
                    Description = "Harry Potter is a film series based on the eponymous novels by J. K. Rowling. The series is produced and distributed by Warner Bros. Pictures and consists of eight fantasy films."
                }
            };
            return franchises;
        }

        /// <summary>
        /// Method to get all movies
        /// </summary>
        /// <returns>Returns a list of movie objects</returns>
        public static List<Movie> GetMovies()
        {
            List<Movie> movies = new List<Movie>()
            {
                new Movie() {
                    Id = 1,
                    Title = "Spider-Man: Far From Home",
                    Genre = "Action, Science fiction, Superhero Movie",
                    ReleaseYear = 2021,
                    Director = "Jon Watts",
                    Picture = "https://en.wikipedia.org/wiki/Spider-Man:_No_Way_Home#/media/File:Spider-Man_No_Way_Home_poster.jpg",
                    Trailer = "https://www.youtube.com/watch?v=JfVOs4VSpmA",
                    FranchiseId = 1
                },


                new Movie() {
                    Id = 2,
                    Title = "Avengers: Endgame",
                    Genre = "Action, Adventure, Fantasy, Science fiction",
                    ReleaseYear = 2019,
                    Director = "Anthony Russo & Joe Russo",
                    Picture = "https://en.wikipedia.org/wiki/Avengers:_Endgame#/media/File:Avengers_Endgame_poster.jpg",
                    Trailer = "https://www.youtube.com/watch?v=TcMBFSGVi1c",
                    FranchiseId = 1
                },

                new Movie() {
                    Id = 3,
                    Title = "Harry Potter and the Philosopher's Stone",
                    Genre = "Fantasy, Adventure, Narrative",
                    ReleaseYear = 2001,
                    Director = "Chris Colombus",
                    Picture = "https://en.wikipedia.org/wiki/Harry_Potter_and_the_Philosopher%27s_Stone_(film)#/media/File:Harry_Potter_and_the_Philosopher's_Stone_banner.jpg",
                    Trailer = "https://www.youtube.com/watch?v=VyHV0BRtdxo",
                    FranchiseId = 3
                },

                new Movie() {
                    Id = 4,
                    Title = "The Hobbit: The Desolation of Smaug",
                    Genre = "Fantasy, Adventure, Drama",
                    ReleaseYear = 2013,
                    Director = "Peter Jackson",
                    Picture = "https://en.wikipedia.org/wiki/The_Hobbit:_The_Desolation_of_Smaug#/media/File:The_Hobbit_-_The_Desolation_of_Smaug_theatrical_poster.jpg",
                    Trailer = "https://www.youtube.com/watch?v=OPVWy1tFXuc",
                    FranchiseId = 2
                }
            };
            return movies;
        }

        /// <summary>
        /// Method to get all characters
        /// </summary>
        /// <returns>Returns a list of character objects</returns>
        public static List<Character> GetCharacters()
        {
            List<Character> characters = new List<Character>()
            {
                new Character() {
                    Id = 1, 
                    Name = "Peter Parker", 
                    Alias = "Spider-Man", 
                    Gender = "Male", 
                    Picture = "https://filmparadiset.se/wp-content/uploads/2021/11/Spider-Man-No-Way-Home-Featured.jpg"
                },

                new Character() {
                    Id = 2,
                    Name = "Albus Percival Wulfric Brian Dumbledore", 
                    Alias = "Dumbledore", 
                    Gender = "Male", 
                    Picture = "https://upload.wikimedia.org/wikipedia/en/f/fe/Dumbledore_and_Elder_Wand.jpg" 
                },

                new Character() {
                    Id = 3,
                    Name = "Radagast", 
                    Alias = "The Brown Wizard", 
                    Gender = "Male", 
                    Picture = "https://static.wikia.nocookie.net/lotr/images/1/11/Radagast_the_Brown.jpg/revision/latest?cb=20190625121542" 
                }
            };
            return characters;
        }
    }
}
