# syntax=docker/dockerfile:1
FROM mcr.microsoft.com/dotnet/sdk:5.0 as build-env
## Need the following three lines to avoid getting a certificate issue
#COPY cert1.cer /usr/share/ca-certificates/
#RUN echo cert1.cer >> /etc/ca-certificates.conf
#RUN update-ca-certificates

WORKDIR /app
# copy csproject and restore as distinct layers
COPY *.csproj ./
RUN dotnet restore
# copy everythign else and build
COPY . ./
RUN dotnet publish -c Release -o out
# build runtime image
FROM mcr.microsoft.com/dotnet/aspnet:5.0 
WORKDIR /app
COPY --from=build-env /app/out .
ENTRYPOINT ["dotnet", "Assignment3.dll"]
