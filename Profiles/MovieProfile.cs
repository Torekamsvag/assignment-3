﻿using Assignment3.DTO.Character;
using Assignment3.DTO.Movie;
using Assignment3.Model;
using AutoMapper;
using System.Linq;

namespace Assignment3.Profiles
{
    public class MovieProfile : Profile   // Inheriting from the Profile class
    {
        public MovieProfile()
        {
            // Mapping from Movie to MovieReadDTO
            CreateMap<Movie, MovieReadDTO>()
                 // Turning related movies into int arrays
                 .ForMember(cdto => cdto.Characters, opt => opt
                 // Mapping from the Id property of the Characters collection in the Movie class
                 .MapFrom(c => c.Characters.Select(c => c.Id).ToArray()))
                 .ForMember(fdto => fdto.Franchise, opt => opt
                 // Mapping from the FranchiseID property in the Movie class
                 .MapFrom(f => f.FranchiseId))
                 .ReverseMap();

            // Enabling two-way mapping between MovieCreateDTO and Movie by using ReverseMap
            CreateMap<MovieCreateDTO, Movie>()
                .ReverseMap();
            // Enabling two-way mapping between MovieEditDTO and Movie by using ReverseMap
            CreateMap<MovieEditDTO, Movie>()
                .ReverseMap();
        }
    }
}
