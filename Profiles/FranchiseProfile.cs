﻿using Assignment3.DTO.Franchise;
using Assignment3.Model;
using AutoMapper;
using System.Linq;

namespace Assignment3.Profiles
{
    public class FranchiseProfile : Profile  // Inheriting from the profile class
    {
        public FranchiseProfile()
        {
            // Mapping from Franchise to FranchiseReadDTO
            CreateMap<Franchise, FranchiseReadDTO>()
                .ForMember(fdto => fdto.Movies, opt => opt
                // Map from the ID property of the Movies collection in the Franchise class
                .MapFrom(f => f.Movies.Select(f => f.Id).ToArray()));
            
            // Mapping from FranchiseCreateDTO to Franchise
            CreateMap<FranchiseCreateDTO, Franchise>();
            // Mapping from FranchiseEditDTO to Franchise
            CreateMap<FranchiseEditDTO, Franchise>();
        }
    }
}
