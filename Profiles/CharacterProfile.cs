﻿using Assignment3.DTO.Character;
using Assignment3.Model;
using AutoMapper;
using System.Linq;

namespace Assignment3.Profiles
{
    public class CharacterProfile : Profile  // Inheriting from the profile class
    {
        public CharacterProfile()
        {   
            // Defining a mapping from Character to CharacterReadDTO
            CreateMap<Character, CharacterReadDTO>()
                .ForMember(cdto => cdto.Movies, opt => opt
                // Map from the Id property of the Movies collection in Character
                .MapFrom(c => c.Movies.Select(m => m.Id).ToArray()))
                // Enabling two-way mapping between Character and CharacterReadDTO
                .ReverseMap();

            // Defing a mapping from Character to CharacterCreateDTO and enabling two-way mapping
            CreateMap<Character, CharacterCreateDTO>()
                .ReverseMap();
            // Defing a mapping from Character to CharacterEditDTO and enabling two-way mapping
            CreateMap<Character, CharacterEditDTO>()
                .ReverseMap();
        }
    }
}
