﻿using System.Collections;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace Assignment3.Model
{
    /// <summary>
    /// Specifying the properties for the character class
    /// </summary>
    public class Character
    {   
        // Primary key is automatically assigned by using the name "Id".
        public int Id { get; set; }
        [Required]
        public string Name { get; set; }
        public string Alias { get; set; }
        public string Gender { get; set; }
        public string Picture { get; set; }
        // Collection navigation property
        public ICollection<Movie> Movies { get; set; }
    }
}
