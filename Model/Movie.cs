﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace Assignment3.Model
{
    /// <summary>
    /// Specifying the properties for the movie class
    /// </summary>
    public class Movie
    {
        // Primary key is automatically assigned.
        public int Id { get; set; }
        [Required]
        [StringLength(80)]
        public string Title { get; set; }
        public string Genre { get; set; }
        [Range(1900,2023)]
        public int ReleaseYear { get; set; }
        public string Director { get; set; }
        public string Picture { get; set; }
        public string Trailer { get; set; }
        // Specifying the foreign key property
        public int? FranchiseId { get; set; }
        // Setting the navigation properties
        public Franchise Franchise { get;} // Inverse navigation property for the Franchise class
        public ICollection<Character> Characters { get; set; }  // Navigation collection property
    }
}
