﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace Assignment3.Model
{
    /// <summary>
    /// Specifying the properties for the franchise class
    /// </summary>
    public class Franchise
    {
        // Primary key is automatically assigned
        public int Id { get; set; }
        [Required]
        public string Name { get; set; }
        [StringLength (300)]
        public string Description { get; set; }
        // Collection navigation property
        public ICollection<Movie> Movies { get; set; }
    }
}
