﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace Assignment3.DTO.Character
{
    public class CharacterReadDTO
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public string Alias { get; set; }
        public string Gender { get; set; }
        public string Picture { get; set; }
        public List<int> Movies { get; set; }
    }
}
