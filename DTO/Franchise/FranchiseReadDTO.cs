﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace Assignment3.DTO.Franchise
{
    public class FranchiseReadDTO
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public string Description { get; set; }
        public List<int> Movies { get; set; }
    }
}
