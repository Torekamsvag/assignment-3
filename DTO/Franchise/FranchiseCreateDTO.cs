﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace Assignment3.DTO.Franchise
{
    public class FranchiseCreateDTO
    {
        public string Name { get; set; }
        public string Description { get; set; }
    }
}
