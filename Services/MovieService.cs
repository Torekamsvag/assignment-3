﻿using Assignment3.Data;
using Assignment3.Model;
using Microsoft.EntityFrameworkCore;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Assignment3.Services
{
    /// <summary>
    /// This class implements the IMovieService interface and provides CRUD operations for the Movie model
    /// </summary>
    public class MovieService : IMovieService
    {
        // FilmUniverseDbContext instance to communicate with the database
        private readonly FilmUniverseDbContext _context;

        // Constructor to initialize FilmUniverseDbContext instance
        public MovieService(FilmUniverseDbContext context)
        {
            _context = context;
        }

        /// <summary>
        /// Method to add a new Movie object to the database
        /// </summary>
        /// <param name="movie">Takes in a movie class object</param>
        /// <returns></returns>
        public async Task<Movie> AddMovieAsync(Movie movie)
        {
            _context.Movies.Add(movie);
            await _context.SaveChangesAsync();

            return movie;
        }

        /// <summary>
        /// Method to delete a movie from the database
        /// </summary>
        /// <param name="id">Takes in an integer value as the id</param>
        /// <returns></returns>
        public async Task DeleteMovieAsync(int id)
        {
            var movie = await _context.Movies.FindAsync(id);
            _context.Movies.Remove(movie);
            await _context.SaveChangesAsync();
        }

        /// <summary>
        /// Method to retrieve all movies from the database
        /// </summary>
        /// <returns></returns>
        public async Task<IEnumerable<Movie>> GetAllMoviesAsync()
        {
            return await _context.Movies
                .Include(m => m.Characters)
                .ToListAsync();
        }

        /// <summary>
        /// Method to get all characters in a movie
        /// </summary>
        /// <param name="movieId">Takes in an integer value as the movieId</param>
        /// <returns></returns>
        public async Task<IEnumerable<Character>> GetCharactersByMovieAsync(int movieId)
        {
            Movie movieToCheck = await _context.Movies
                .Include(m => m.Characters)
                .FirstAsync(m => m.Id == movieId);

            List<Character> charactersInMovie = new List<Character>();

            foreach (Character c in movieToCheck.Characters)
            {
                charactersInMovie.Add(c);
            }
            return charactersInMovie;
        }

        /// <summary>
        /// Method to retrieve a specific movie from the database
        /// </summary>
        /// <param name="id">Takes in an integer value as the id</param>
        /// <returns></returns>
        public async Task<Movie> GetSpecificMovieAsync(int id)
        {
            return await _context.Movies
                .Include(m => m.Characters)
                .FirstAsync(c => c.Id == id);
        }

        /// <summary>
        /// Method to check if a movie with the given id exists in the database
        /// </summary>
        /// <param name="id">Takes in an integer value as the id</param>
        /// <returns>Returns a boolean value, which returns true or false</returns>
        public bool MovieExists(int id)
        {
            return _context.Movies.Any(c => c.Id == id);
        }

        /// <summary>
        /// Method to update a movie in the database
        /// </summary>
        /// <param name="movie">Takes in a movie class object</param>
        /// <returns></returns>
        public async Task UpdateMovieAsync(Movie movie)
        {
            _context.Entry(movie).State = EntityState.Modified;
            await _context.SaveChangesAsync();
        }

        /// <summary>
        /// Method to update characters in a movie 
        /// </summary>
        /// <param name="MovieId">Takes in a MovieId parameter</param>
        /// <param name="characters">Takes in an integer array of character Id's</param>
        /// <returns></returns>
        /// <exception cref="KeyNotFoundException"></exception>
        public async Task UpdateMovieCharactersAsync(int MovieId, int[] characters)
        {
            Movie movieToUpdateCharacters = await _context.Movies
                .Include(m => m.Characters)
                .Where(m => m.Id == MovieId)
                .FirstAsync();

            List<Character> chars = new();
            foreach (int characterId in characters)
            {
                Character charObj = await _context.Characters.FindAsync(characterId);
                if (charObj == null)
                {
                    throw new KeyNotFoundException();
                }
                chars.Add(charObj);
            }
            movieToUpdateCharacters.Characters = chars;
            await _context.SaveChangesAsync();
        }
    }
}

