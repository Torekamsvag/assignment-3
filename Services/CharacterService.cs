﻿using Assignment3.Data;
using Assignment3.Model;
using Microsoft.EntityFrameworkCore;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Assignment3.Services
{
    /// <summary>
    /// This class implements the ICharacterService interface and provides CRUD operations for the Character model
    /// </summary>
    public class CharacterService : ICharacterService
    {
        // FilmUniverseDbContext instance to communicate with the database
        private readonly FilmUniverseDbContext _context;

        // Constructor to initialize FilmUniverseDbContext instance
        public CharacterService(FilmUniverseDbContext context)
        {
            _context = context;
        }

        /// <summary>
        /// Method to add a new Character object to the database
        /// </summary>
        /// <param name="character">Takes in a character class object</param>
        /// <returns></returns>
        public async Task<Character> AddCharacterAsync(Character character)
        {
            _context.Characters.Add(character);
            await _context.SaveChangesAsync();

            return character;
        }

        /// <summary>
        /// Method to check if a character with the given id exists in the database
        /// </summary>
        /// <param name="id">Takes in an integer valuse as the id</param>
        /// <returns>Returns a boolean value, which returns true or false</returns>
        public bool CharacterExists(int id)
        {
            return _context.Characters.Any(c => c.Id == id);
        }

        /// <summary>
        /// Method to delete a character from the database
        /// </summary>
        /// <param name="id">Takes in an integer valuse as the id</param>
        /// <returns></returns>
        public async Task DeleteCharacterAsync(int id)
        {
            var character = await _context.Characters.FindAsync(id);
            _context.Characters.Remove(character);
            await _context.SaveChangesAsync();
        }

        /// <summary>
        /// Method to retrieve all characters from the database
        /// </summary>
        /// <returns></returns>
        public async Task<IEnumerable<Character>> GetAllCharactersAsync()
        {
            return await _context.Characters
                .Include(c => c.Movies)
                .ToListAsync();
        }

        /// <summary>
        /// Method to retrieve a specific character from the database
        /// </summary>
        /// <param name="id">Takes in an integer valuse as the id</param>
        /// <returns></returns>
        public async Task<Character> GetSpecificCharacterAsync(int id)
        {
            return await _context.Characters
                .Include(c => c.Movies)
                .FirstAsync(c => c.Id == id);
        }

        /// <summary>
        /// Method to update a character in the database
        /// </summary>
        /// <param name="character"></param>
        /// <returns></returns>
        public async Task UpdateCharacterAsync(Character character)
        {
            _context.Entry(character).State = EntityState.Modified;
            await _context.SaveChangesAsync();
        }
    }
}
