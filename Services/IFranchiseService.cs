﻿using Assignment3.Model;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace Assignment3.Services
{
    /// <summary>
    /// Interface service which specifies method signatures related to franchises
    /// </summary>
    public interface IFranchiseService
    {
        public Task<IEnumerable<Franchise>> GetAllFranchisesAsync();
        public Task<Franchise> GetSpecificFranchiseAsync(int id);
        public Task<IEnumerable<Movie>> GetMoviesByFranchiseAsync(int id);
        public Task<IEnumerable<Character>> GetCharactersByFranchiseAsync(int franchiseId);
        public Task<Franchise> AddFranchiseAsync(Franchise character);
        public Task UpdateFranchiseAsync(Franchise character);
        public Task UpdateFranchiseMoviesAsync(int FranchiseId, int[] movies);
        public Task DeleteFranchiseAsync(int id);
        public bool FranchiseExists(int id);
        
    }
}
