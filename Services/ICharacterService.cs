﻿using Assignment3.Model;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace Assignment3.Services
{
    /// <summary>
    /// Interface service which specifies method signatures related to characters
    /// </summary>
    public interface ICharacterService
    {
        public Task<IEnumerable<Character>> GetAllCharactersAsync();
        public Task<Character> GetSpecificCharacterAsync(int id);
        public Task<Character> AddCharacterAsync(Character character);
        public Task UpdateCharacterAsync(Character character);
        public Task DeleteCharacterAsync(int id);
        public bool CharacterExists(int id);
    }
}
