﻿using Assignment3.Data;
using Assignment3.DTO.Movie;
using Assignment3.Model;
using Microsoft.EntityFrameworkCore;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Assignment3.Services
{
    /// <summary>
    /// This class implements the IFranchiseService interface and provides CRUD operations for the Franchise model
    /// </summary>
    public class FranchiseService : IFranchiseService
    {
        // FilmUniverseDbContext instance to communicate with the database
        private readonly FilmUniverseDbContext _context;

        // Constructor to initialize FilmUniverseDbContext instance
        public FranchiseService(FilmUniverseDbContext context)
        {
            _context = context;
        }


        /// <summary>
        /// Method to add a new Franchise object to the database
        /// </summary>
        /// <param name="franchise">Takes in a franchise class object</param>
        /// <returns>Returns a franchise class object</returns>
        public async Task<Franchise> AddFranchiseAsync(Franchise franchise)
        {
            _context.Franchises.Add(franchise);
            await _context.SaveChangesAsync();

            return franchise;
        }

        /// <summary>
        /// Method to delete a franchise from the database
        /// </summary>
        /// <param name="id">Takes in an integer valuse as the id</param>
        /// <returns></returns>
        public async Task DeleteFranchiseAsync(int id)
        {
            var franchise = await _context.Franchises.FindAsync(id);
            _context.Franchises.Remove(franchise);
            await _context.SaveChangesAsync();
        }

        /// <summary>
        /// Method to check if a franchise with the given id exists in the database
        /// </summary>
        /// <param name="id">Takes in an integer valuse as the id</param>
        /// <returns>Returns a boolean value, which returns true or false</returns>
        public bool FranchiseExists(int id)
        {
            return _context.Franchises.Any(f => f.Id == id);
        }

        /// <summary>
        /// Method to retrieve all franchises from the database
        /// </summary>
        /// <returns></returns>
        public async Task<IEnumerable<Franchise>> GetAllFranchisesAsync()
        {
            return await _context.Franchises
                .Include(f => f.Movies)
                .ToListAsync();
              
        }

        /// <summary>
        /// Method to get all movies from a given franchise
        /// </summary>
        /// <param name="id">Takes in an integer valuse as the id</param>
        /// <returns></returns>
        public async Task<IEnumerable<Movie>> GetMoviesByFranchiseAsync(int id)
        {
            return await _context.Movies
                .Where(m => m.FranchiseId == id)
                .ToListAsync();
        }
        
        /// <summary>
        /// Method to get all characters in a franchise
        /// </summary>
        /// <param name="franchiseId"></param>
        /// <returns></returns>
        public async Task<IEnumerable<Character>> GetCharactersByFranchiseAsync(int franchiseId)
        {
            Franchise franchiseToCheck = await _context.Franchises
                .Include(f => f.Movies).ThenInclude(m => m.Characters)
                .FirstAsync(f => f.Id == franchiseId);

            List<Character> charactersInFranchise = new List<Character>();

            foreach (Movie m in franchiseToCheck.Movies)
            {
                foreach (Character c in m?.Characters)
                {
                   charactersInFranchise.Add(c);
                }
            }
            return charactersInFranchise;
        }

        /// <summary>
        /// Method to retrieve a specific franchise from the database
        /// </summary>
        /// <param name="id">Takes in an integer valuse as the id</param>
        /// <returns></returns>
        public async Task<Franchise> GetSpecificFranchiseAsync(int id)
        {
            return await _context.Franchises
                .Include(f => f.Movies)
                .FirstAsync(f => f.Id == id);
        }

        /// <summary>
        /// Method to update a franchise in the database
        /// </summary>
        /// <param name="franchise"></param>
        /// <returns></returns>
        public async Task UpdateFranchiseAsync(Franchise franchise)
        {
            _context.Entry(franchise).State = EntityState.Modified;
            await _context.SaveChangesAsync();
        }

        /// <summary>
        /// Method to update movies in a franchise
        /// </summary>
        /// <param name="franchiseId"></param>
        /// <param name="movies"></param>
        /// <returns></returns>
        /// <exception cref="KeyNotFoundException"></exception>
        public async Task UpdateFranchiseMoviesAsync(int franchiseId, int[] movies)
        {
            Franchise franchiseToUpdateMovies = await _context.Franchises
                    .Include(f => f.Movies)
                    .Where(f => f.Id == franchiseId)
                    .FirstAsync();

            List<Movie> films = new();
            foreach (int movieId in movies)
            {
                Movie movObj = await _context.Movies.FindAsync(movieId);
                if (movObj == null)
                {
                    throw new KeyNotFoundException();
                }
                films.Add(movObj);
            }
            franchiseToUpdateMovies.Movies = films;
            await _context.SaveChangesAsync();
        }
    }
}
