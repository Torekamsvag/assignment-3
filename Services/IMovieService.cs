﻿using Assignment3.Model;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace Assignment3.Services
{   
    /// <summary>
    /// Interface service which specifies method signatures related to movies
    /// </summary>
    public interface IMovieService
    {
        public Task<IEnumerable<Movie>> GetAllMoviesAsync();
        public Task<Movie> GetSpecificMovieAsync(int id);
        public Task<Movie> AddMovieAsync(Movie movie);
        public Task UpdateMovieAsync(Movie movie);
        public Task DeleteMovieAsync(int id);
        public bool MovieExists(int id);
        public Task UpdateMovieCharactersAsync(int movieId, int[] characters);
        public Task<IEnumerable<Character>> GetCharactersByMovieAsync(int movieId);
    }
}
