﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Assignment3.Data;
using Assignment3.Model;
using Assignment3.DTO.Movie;
using AutoMapper;
using Assignment3.Services;
using Assignment3.DTO.Character;
using System.Net.Mime;

namespace Assignment3.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    [Produces(MediaTypeNames.Application.Json)]
    [Consumes(MediaTypeNames.Application.Json)]
    [ApiConventionType(typeof(DefaultApiConventions))]
    public class MoviesController : ControllerBase
    {
        private readonly IMapper _mapper;
        private readonly IMovieService _movieService;

        public MoviesController(IMapper mapper, IMovieService movieService)
        {
            _mapper = mapper;
            _movieService = movieService;
        }

        /// <summary>
        /// Get all movies from database
        /// </summary>
        /// <returns>Returns a list of Movie objects</returns>
        [HttpGet]
        public async Task<ActionResult<IEnumerable<MovieReadDTO>>> GetMovies()
        {
            return _mapper.Map<List<MovieReadDTO>>(await _movieService.GetAllMoviesAsync());
        }

        /// <summary>
        /// Get a specific movie based on their id
        /// </summary>
        /// <param name="id">Id of the movie to get</param>
        /// <returns></returns>
        [HttpGet("{id}")]
        public async Task<ActionResult<MovieReadDTO>> GetMovie(int id)
        {
            Movie movie = await _movieService.GetSpecificMovieAsync(id);

            if (movie == null)
            {
                return NotFound();
            }

            return _mapper.Map<MovieReadDTO>(movie);
        }

        /// <summary>
        /// Get all characters who play in a specific movie
        /// </summary>
        /// <param name="movieId">Id of the movie to get characters from</param>
        /// <returns></returns>
        [HttpGet("characters")]
        public async Task<ActionResult<IEnumerable<CharacterReadDTO>>> GetCharactersByMovie(int movieId)
        {
            if (!_movieService.MovieExists(movieId))
            {
                return NotFound();
            }
            try
            {
                return _mapper.Map<List<CharacterReadDTO>>(await _movieService.GetCharactersByMovieAsync(movieId));
            }
            catch (KeyNotFoundException)
            {
                return BadRequest("Invalid movie.");
            }
        }

        /// <summary>
        /// Add a movie to the database
        /// </summary>
        /// <param name="dtoMovie">The movie object to add to the database</param>
        /// <returns></returns>
        [HttpPost]
        public async Task<ActionResult<Movie>> PostMovie(MovieCreateDTO dtoMovie)
        {
            Movie domainMovie = _mapper.Map<Movie>(dtoMovie);

            domainMovie = await _movieService.AddMovieAsync(domainMovie);

            return CreatedAtAction("GetMovie",
                new { id = domainMovie.Id },
                _mapper.Map<MovieReadDTO>(domainMovie));
        }

        /// <summary>
        /// Edit a movie in the database
        /// </summary>
        /// <param name="id">Id of the movie to edit</param>
        /// <param name="dtoMovie">The modified movie object</param>
        /// <returns></returns>
        [HttpPut("{id}")]
        public async Task<IActionResult> PutMovie(int id, MovieEditDTO dtoMovie)
        {
            if (id != dtoMovie.Id)
            {
                return BadRequest();
            }

            if (!_movieService.MovieExists(id))
            {
                return NotFound();
            }
            Movie domainMovie = _mapper.Map<Movie>(dtoMovie);
            await _movieService.UpdateMovieAsync(domainMovie);

            return NoContent();
        }

        /// <summary>
        /// Update characters of a movie in the database
        /// </summary>
        /// <param name="MovieId">The id of the movie to update characters for</param>
        /// <param name="characters">Array of ids for the characters</param>
        /// <returns></returns>
        [HttpPut("characters")]
        public async Task<IActionResult> UpdateMovieCharacters(int MovieId, int[] characters)
        {
            if (!_movieService.MovieExists(MovieId))
            {
                return NotFound();
            }

            try
            {
                await _movieService.UpdateMovieCharactersAsync(MovieId, characters);
            }
            catch (KeyNotFoundException)
            {
                return BadRequest("Invalid characters.");
            }

            return NoContent();
        }

        /// <summary>
        /// Delete a movie from the database
        /// </summary>
        /// <param name="id">Id of the movie to delete</param>
        /// <returns></returns>
        [HttpDelete("{id}")]
        public async Task<IActionResult> DeleteMovie(int id)
        {
            if (!_movieService.MovieExists(id))
            {
                return NotFound();
            }

            await _movieService.DeleteMovieAsync(id);

            return NoContent();
        }
    }
}
