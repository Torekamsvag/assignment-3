﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Assignment3.Data;
using Assignment3.Model;
using Assignment3.Services;
using AutoMapper;
using Assignment3.DTO.Franchise;
using Assignment3.DTO.Character;
using Assignment3.DTO.Movie;
using System.Net.Mime;

namespace Assignment3.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    [Produces(MediaTypeNames.Application.Json)]
    [Consumes(MediaTypeNames.Application.Json)]
    [ApiConventionType(typeof(DefaultApiConventions))]
    public class FranchisesController : ControllerBase
    {
        private readonly IMapper _mapper;
        private readonly IFranchiseService _franchiseService;

        public FranchisesController(IMapper mapper, IFranchiseService franchiseService)
        {
            _mapper = mapper;
            _franchiseService = franchiseService;
        }

        /// <summary>
        /// Get all franchises from database
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        public async Task<ActionResult<IEnumerable<FranchiseReadDTO>>> GetFranchises()
        {
            return _mapper.Map<List<FranchiseReadDTO>>(await _franchiseService.GetAllFranchisesAsync());
        }

        /// <summary>
        /// Get a specific franchise based on their id
        /// </summary>
        /// <param name="id">The id of the franchise to retrieve</param>
        /// <returns></returns>
        [HttpGet("{id}")]
        public async Task<ActionResult<FranchiseReadDTO>> GetFranchise(int id)
        {
            Franchise franchise = await _franchiseService.GetSpecificFranchiseAsync(id);

            if (franchise == null)
            {
                return NotFound();
            }

            return _mapper.Map<FranchiseReadDTO>(franchise);
        }

        /// <summary>
        /// Edit a franchise in the database
        /// </summary>
        /// <param name="id">The id of the franchise to edit</param>
        /// <param name="dtoFranchise">A modified franchise object</param>
        /// <returns></returns>
        [HttpPut("{id}")]
        public async Task<IActionResult> PutFranchise(int id, FranchiseEditDTO dtoFranchise)
        {
            if (id != dtoFranchise.Id)
            {
                return BadRequest();
            }

            if (!_franchiseService.FranchiseExists(id))
            {
                return NotFound();
            }

            Franchise domainFranchise = _mapper.Map<Franchise>(dtoFranchise);
            await _franchiseService.UpdateFranchiseAsync(domainFranchise);

            return NoContent();
        }

        /// <summary>
        /// Add a franchise to the database
        /// </summary>
        /// <param name="dtoFranchise">A modified franchise object</param>
        /// <returns></returns>
        [HttpPost]
        public async Task<ActionResult<Franchise>> PostFranchise(FranchiseCreateDTO dtoFranchise)
        {
            Franchise domainFranchise = _mapper.Map<Franchise>(dtoFranchise);

            domainFranchise = await _franchiseService.AddFranchiseAsync(domainFranchise);

            return CreatedAtAction("GetFranchise",
                new { id = domainFranchise.Id },
                _mapper.Map<FranchiseReadDTO>(domainFranchise));
        }

        /// <summary>
        /// Delete a franchise from the database
        /// </summary>
        /// <param name="id">The id of a franchise to delete</param>
        /// <returns></returns>
        [HttpDelete("{id}")]
        public async Task<IActionResult> DeleteFranchise(int id)
        {
            if (!_franchiseService.FranchiseExists(id))
            {
                return NotFound();
            }

            await _franchiseService.DeleteFranchiseAsync(id);

            return NoContent();
        }

        /// <summary>
        /// Update movies in a franchise
        /// </summary>
        /// <param name="franchiseId">The id of the franchise to update</param>
        /// <param name="movies">Integer array of movie id's</param>
        /// <returns></returns>
        [HttpPut("movies")]
        public async Task<IActionResult> UpdateFranchiseMovies(int franchiseId, int[] movies)
        {
            if (!_franchiseService.FranchiseExists(franchiseId))
            {
                return NotFound();
            }
           
            try
            {
                await _franchiseService.UpdateFranchiseMoviesAsync(franchiseId, movies);
            }   
            catch (KeyNotFoundException)
            {
                return BadRequest("Invalid movies.");
            }

            return NoContent(); 
        }

        
        /// <summary>
        /// Get all the movies within a specific franchise
        /// </summary>
        /// <param name="id">The id of the franchise to retrieve from</param>
        /// <returns></returns>
        [HttpGet("{id}/MoviesByFranchise")]
        public async Task<ActionResult<IEnumerable<MovieReadDTO>>> GetMoviesByFranchise(int id)
        {
            if (!_franchiseService.FranchiseExists(id))
            {
                return NotFound();
            }
            
            try
            {
                return _mapper.Map<List<MovieReadDTO>>(await _franchiseService.GetMoviesByFranchiseAsync(id));
            }
            catch (KeyNotFoundException)
            {
                return BadRequest("Invalid franchise.");
            }
        }

        /// <summary>
        /// Get all the characters within a specific franchise
        /// </summary>
        /// <param name="franchiseId">The id of the franchise to retrieve from</param>
        /// <returns></returns>
        [HttpGet("CharactersByFranchise")]
        public async Task<ActionResult<IEnumerable<CharacterReadDTO>>> GetCharactersByFranchise(int franchiseId)
        {
            if (!_franchiseService.FranchiseExists(franchiseId))
            {
                return NotFound();
            }

            try
            {
                return _mapper.Map<List<CharacterReadDTO>>(await _franchiseService.GetCharactersByFranchiseAsync(franchiseId));
            }
            catch (KeyNotFoundException)
            {
                return BadRequest("Invalid franchise.");
            }
        }
    }
}